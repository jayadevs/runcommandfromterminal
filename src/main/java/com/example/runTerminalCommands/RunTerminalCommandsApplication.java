package com.example.runTerminalCommands;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class RunTerminalCommandsApplication implements CommandLineRunner {


	public static void main(String[] args) {
		ApplicationContext context=SpringApplication.run(RunTerminalCommandsApplication.class, args);

	}
	public void run(String... args) throws Exception{
		System.out.println("Starting");
	}
}
