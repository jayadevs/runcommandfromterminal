package com.example.runTerminalCommands.controller;

import com.example.runTerminalCommands.model.DataDictionarySyncRequest;
import com.example.runTerminalCommands.service.DataDictionaryService;
import com.example.runTerminalCommands.service.UIHelper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.io.FileNotFoundException;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


@RestController
public class DataDictionaryController {

    @Autowired
    DataDictionaryService dataDictionaryService;

    @Autowired
    UIHelper uiHelper;

    @Autowired
    ObjectMapper objectMapper;

    @PostMapping(value="sync-data-dictionary")
    public Object syncDataDictionary(DataDictionarySyncRequest dataDictionarySyncRequest) throws UnirestException, JsonProcessingException, FileNotFoundException {
        if(dataDictionaryService.checkValid(dataDictionarySyncRequest))
            return dataDictionaryService.syncDataDictionary(dataDictionarySyncRequest);
        else
            return "Start Date must be before End Date";

    }


    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {

        return builder
                .setConnectTimeout(Duration.ofMillis(3000))
                .setReadTimeout(Duration.ofMillis(3000))
                .build();
    }
    @Bean
    public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        mapper.configure(SerializationFeature.INDENT_OUTPUT,true);
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter(mapper);
        return converter;
    }



}
