package com.example.runTerminalCommands.controller;

import com.example.runTerminalCommands.model.Job;
import com.example.runTerminalCommands.service.FlowOfControl;
import com.example.runTerminalCommands.service.JobManager;
import com.example.runTerminalCommands.service.UIHelper;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class RunCommandController {

    @Autowired
    FlowOfControl controller;
    @Autowired
    UIHelper uiHelper;

    @Autowired
    JobManager jobManager;



    @PostMapping("/execute")
    public Object runCommand(Job job) throws InterruptedException, IOException, ParseException, java.text.ParseException {
        //System.out.println(Arrays.toString(request));

        List<String> input = uiHelper.inputParse(job);

        String instId = input.get(0);
        String startDate = input.get(1);
        String endDate = input.get(2);
        String collection = input.get(3);
        System.out.println(Arrays.toString(new List[]{input}));
        return controller.startJob(instId,startDate,endDate,collection);


    }


    @PostMapping("/executeraw")
    public Object runCommand(@RequestBody String[] request) throws InterruptedException, IOException, ParseException {

        String instId = request[0];
        String startDate = request[1];
        String endDate = request[2];
        String collection = request[3];
        System.out.println(request);
        return controller.startJob(instId,startDate,endDate,collection);


    }

    @GetMapping("/listsubjobs")
    public Object jobDetails(Job job) throws IOException {
//        return (controller.jobDetails(request));
        if(controller.jobDetails(job.getJobuuid())==null)
            return "Job not found";
        return (controller.jobDetails(job.getJobuuid()));

    }

    @GetMapping("/listalljobs")
    public Object allJobDetails() throws IOException {
//        return (controller.jobDetails(request));
        return (jobManager.readGson());
    }

    @GetMapping("/listallrunningjobs")
    public Object allsubJobDetails() throws IOException {
//        return (controller.jobDetails(request));
        return (jobManager.listrunningJobs());

    }
    @GetMapping("/getlogs")
    public Object subJobDetails(String logfile) throws IOException {
//        return (controller.jobDetails(request));
        return (jobManager.getLogs(logfile));

    }
    @GetMapping("/appStatus")
    public Object subJobStatus(String appId) throws IOException {
//        return (controller.jobDetails(request));
        return jobManager.applicationStatus(appId);

    }

    @GetMapping("/listalljobsbytime")
    public Object allJobDetails(long time) throws IOException {
//        return (controller.jobDetails(request));
        return (jobManager.jobByTime(time));

    }
    @GetMapping("/listsubjobsraw")
    public Object jobDetails(@RequestBody int instID) throws IOException {
//        return (controller.jobDetails(request));
        return (controller.jobDetails(instID));

    }
    @PostMapping("/force")
    public Object forceRun(Job job) throws IOException, ParseException {
        if(controller.jobDetails(job.getJobuuid())==null)
            return "Job not found";
        return controller.forceRun(job.getJobuuid());

    }
    @PostMapping("/forceraw")
    public Object forceRun(@RequestBody int jobuid) throws IOException, ParseException {
        if(controller.jobDetails(jobuid)==null)
            return "Job does not exist previously. Please submit job with all parameters.";
        String s = controller.forceRun(jobuid);
        return s;
    }

}
