package com.example.runTerminalCommands.service;

import com.example.runTerminalCommands.model.Job;
import com.example.runTerminalCommands.model.SubJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class SubJobManagement {
    @Autowired
    FindCommand findCommand;
    @Autowired
    LogWatcher logWatcher;
    @Autowired
    JobManager jobManager;
    @Autowired
    ConfigFile configFile;
    public List<SubJob> makeSubJobs(List<String> dates, Job job){
        int subJobCount=dates.size()/2;
        List<SubJob> subJobs = new ArrayList<>();
        for(int i=0;i<subJobCount;i++){
            SubJob subJob=new SubJob(dates.get(2*i),dates.get((2*i)+1),job.getJobuuid());

            subJobs.add(subJob);
        }
        return subJobs;

    }
    public void run(SubJob job,Job mainJob) throws InterruptedException, IOException {
        String file="recon"+job.getJobuid()+"_"+ job.getStartDate()+"_"+job.getEndDate() + ".sh";
        writeScript(file, job.getCommand());
        String[] command = {"/bin/sh",file};
        Process process= Runtime.getRuntime().exec(command);
        Thread thread = new Thread(() -> {
            System.out.println("Watching logs");
            String foundPattern = null;
            try {
                foundPattern = logWatcher.newWatch(configFile.getLogPath(mainJob)+ job.getLogFile(), "application_", 1000, 1200000);
            } catch ( InterruptedException e) {
                e.printStackTrace();
            }
            if (foundPattern != null) {
                String[] words = foundPattern.split(" ");
                for (String word : words) {
                    if (word.contains("application_")) {
                        System.out.println("Found application id for jobID : "+job.getJobuid() + " : " + word);
                        job.setApplicationId(word);
                        try {
                            jobManager.updateAppId(mainJob);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
//
                    }

                }
            }
        });
        thread.start();
        process.waitFor();

    }
    public void writeScript(String file,String script){
        BufferedWriter out = null;

        try {
            FileWriter fstream = new FileWriter(file);
            fstream.write(script);
            fstream.close();
        }

        catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
        }

    }
}
