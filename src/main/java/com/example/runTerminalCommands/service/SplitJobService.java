package com.example.runTerminalCommands.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class SplitJobService {

    public int findDaysBetween(String startDate, String endDate){
        LocalDate initialDate=LocalDate.parse(parseDate(startDate));
        LocalDate finalDate=LocalDate.parse(parseDate(endDate));
        return Period.between(initialDate,finalDate).getDays()+1;
    }
    public String parseDate(String date){
        String parsedDate=date.substring(4,8)+"-"+date.substring(2,4)+"-"+date.substring(0,2);
        return parsedDate;
    }
    public String anotherParseDate(String date){
        String parsedDate=date.substring(8,10)+date.substring(5,7)+date.substring(0,4);
        return parsedDate;
    }
    public String getDateAsString(LocalDate date){
        String tempDate=date.toString();
        String stringDate=tempDate.substring(8,10)+tempDate.substring(5,7)+tempDate.substring(0,4);
        return stringDate;
    }
    public void splitJobs(String startDate, String endDate,int threshHold, int jobLength){
        if(findDaysBetween(startDate,endDate)>threshHold){
            List<String> startDates=dateIterator(startDate,endDate,jobLength);

        }
    }
    public List<String> dateIterator(String startDate, String endDate, int jobLength){
        List<String> dates=new ArrayList<>();
        LocalDate initialDate=LocalDate.parse(parseDate(startDate));
        LocalDate finalDate=LocalDate.parse(parseDate(endDate));
        while(initialDate.isBefore(finalDate) || initialDate.isEqual(finalDate)){
            dates.add(getDateAsString(initialDate));
            if(initialDate.plusDays(jobLength-1).isAfter(finalDate)){
                dates.add(getDateAsString(finalDate));
            }
            else{
                dates.add(getDateAsString(initialDate.plusDays(jobLength-1)));
            }
            initialDate=initialDate.plusDays(jobLength-1);
        }
        return dates;


    }



}
