package com.example.runTerminalCommands.service;

import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Stack;

@Service
public class LogWatcher {
    private Stack<String> logStack;
    public String watch(String logFile, String last,String pattern,long watchInterval, long timeOutInterval) throws FileNotFoundException, InterruptedException {
        String returnable;
        logStack=new Stack<>();
        logStack.push(last);
        long start_time=System.currentTimeMillis();
        String lastLine=last;
        while (System.currentTimeMillis()<start_time+timeOutInterval){
            String line=lastLine(logFile);
            if(!line.equals(lastLine)){
                returnable=searchIn(line,lastLine,logFile,pattern);
                if(returnable!=null){
                    return returnable;
                }

            }
            Thread.sleep(watchInterval);
        }
        return null;
    }
    public String lastLine(String logFile) throws FileNotFoundException {
        String line="";
        File file = new File(logFile);
        Scanner sc = new Scanner(file);
        while (sc.hasNextLine()){
            line=sc.nextLine();

        }
        return line;


    }
    public String searchIn(String line, String lastLine,String logFile, String pattern) throws FileNotFoundException {
        File file = new File(logFile);
        Scanner sc = new Scanner(file);
        while (sc.hasNextLine()){
            if(sc.nextLine().matches(lastLine)){
                while (sc.hasNextLine()) {
                    String tempLine = sc.nextLine();
                    if (tempLine.contains(pattern)) {
                        return tempLine;
                    }
                }
            }
        }
        return null;

    }
    public String searchLast(String logFile, String pattern) throws FileNotFoundException {
        File file = new File(logFile);
        Scanner sc = new Scanner(file);
        String lastPattern="";
        while (sc.hasNextLine()){
            String line=sc.nextLine();
            if(line.contains(pattern)){
                lastPattern=line;
            }
        }
        return lastPattern;
    }
    public String search(String logFile, String pattern){
        File file = new File(logFile);
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                if (line.contains(pattern)) {
                    return line;
                }
            }

        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
      }
      public String newWatch(String logFile,String pattern,long watchInterval, long timeOutInterval) throws InterruptedException {
        String returnable;
        long start_time=System.currentTimeMillis();
          while (System.currentTimeMillis()<start_time+timeOutInterval){

                  returnable=search(logFile,pattern);
                  if(returnable!=null){
                      return returnable;
                  }


              Thread.sleep(watchInterval);
          }
          return null;
      }

    }
