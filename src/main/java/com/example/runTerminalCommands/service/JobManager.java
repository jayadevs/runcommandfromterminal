package com.example.runTerminalCommands.service;

import com.example.runTerminalCommands.model.Job;
import com.example.runTerminalCommands.model.JobList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class JobManager {
    public Job makeJob(String instId, String startDate, String endDate, String collection){
        Job job=new Job(instId,startDate,endDate,collection);

        return job;

    }
    public int checkExists(Job job) throws IOException {
        System.out.println("Checking if job with jobID : " + job.getJobuuid() + " has already been logged.");
        if(readGson()!=null) {

            List<Job> jobs = readGson();
            for(Job job1 : jobs){
                if(job1.equals(job)){
                    System.out.println("job already present");
                    return job.getJobuuid();
                }
            }
        }
        return 0;
    }

    public synchronized int gsonLogger(Job job) throws IOException {
        List<Job> jobs=new ArrayList<>();
        JobList jobList = new JobList();
        if(readGson()!=null) {
            jobs = readGson();
            jobs.add(job);
            jobList.setJobList(jobs);
        }

            //jobList.add(job);
            Gson gson = new GsonBuilder()
                    .setPrettyPrinting()
                    .create();
//        String json = gson.toJson(jobList);
//        System.out.println(json);
            FileWriter writer = new FileWriter("ReconJobLogs.json");
            gson.toJson(jobList, writer);
            writer.close();

        return job.getJobuuid();
    }
    public synchronized void updateAppId(Job job) throws IOException {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
        List<Job> jobs=new ArrayList<>();
        jobs = readGson();
        JobList jobList = new JobList();
        jobList.setJobList(jobs);
        List<Job> jobListLocalCopy = new ArrayList<>(jobList.getJobList());
        for(Job job1 : jobListLocalCopy){
            if(job1.equals(job)){
                jobList.remove(job1);
                jobList.add(job);
            }
        }
        writeGSON(jobList);

    }
    public List<Job> readGson() throws IOException {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
        File file = new File("ReconJobLogs.json");
        if(!file.exists()) {
            return null;
        }
        try {
            FileReader reader = new FileReader("ReconJobLogs.json");
            JobList jobList = new JobList();
            jobList=gson.fromJson(reader,JobList.class);
            reader.close();
            System.out.println("Reading ReconJobLogs to check if job is present");
            return jobList.getJobList();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return  null;

    }
    public String getLogs(String logfile){
        try {
            FileReader reader = new FileReader("reconLogs/" + logfile);
            BufferedReader anotherReader = new BufferedReader(reader);
            StringBuffer sb = new StringBuffer();
            String line;
            while((line=anotherReader.readLine())!=null){
                sb.append(line);
                sb.append("\n");
            }
            reader.close();
            return sb.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public synchronized void writeGSON(JobList jobList) throws IOException {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
        String json = gson.toJson(jobList);
        //System.out.println(json);
        FileWriter writer = new FileWriter("ReconJobLogs.json");
        gson.toJson(jobList,writer);
        writer.close();
    }

    public int listJobs() throws IOException {
        String command="yarn application -list";
        Process process = Runtime.getRuntime().exec(command);
        BufferedReader anotherReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line;
        while ((line = anotherReader.readLine()) !=  null) {

            System.out.println(line);
            if (line.contains("Total")) {
                return Integer.parseInt(line.substring(line.length() - 1));

            }
        }
        return -1;
    }
    //for getting submitted subjobs
    public String listrunningJobs() throws IOException {
        String command="yarn application -list";
        Process process = Runtime.getRuntime().exec(command);
        BufferedReader anotherReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line;
        StringBuilder returnable = null;
        while ((line = anotherReader.readLine()) !=  null) {

            returnable = (returnable == null ? new StringBuilder("null") : returnable).append(line).append("\n");

            }
        return returnable == null ? null : returnable.toString();

    }
    public String applicationStatus(String appId) throws IOException {
        String command="yarn application -status " + appId;
        Process process = Runtime.getRuntime().exec(command);
        BufferedReader anotherReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line;
        StringBuilder returnable = null;
        while ((line = anotherReader.readLine()) !=  null) {

            returnable = (returnable == null ? new StringBuilder("null") : returnable).append(line).append("\n");

        }
        return returnable == null ? null : returnable.toString();

    }
    //For getting jobs submitted after the given time
    public List<Job> jobByTime(long time){
        try {
            List<Job> jobs = readGson();
            List<Job> jobs1 = new ArrayList<>();
            for(Job job : jobs){
                if(job.getTimeOfSubmission()>time)
                    jobs1.add(job);
            }
            return jobs1;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
