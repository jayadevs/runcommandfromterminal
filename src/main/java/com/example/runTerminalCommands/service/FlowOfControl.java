package com.example.runTerminalCommands.service;

import com.example.runTerminalCommands.model.Job;
import com.example.runTerminalCommands.model.JobList;
import com.example.runTerminalCommands.model.SubJob;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class FlowOfControl {
    @Autowired
    JobManager jobManager;
    @Autowired
    SplitJobService splitJobService;
    @Autowired
    SubJobManagement subJobManagement;
    @Autowired
    FindCommand findCommand;

    public String  startJob(String instId, String startDate, String endDate, String collection) throws IOException, ParseException {
        Job job=jobManager.makeJob(instId,startDate,endDate, collection);

        if(jobManager.checkExists(job)!=0){
            return "Job already logged with Job ID : " + jobManager.checkExists(job);
        }

        List<String> dates = splitJobService.dateIterator(job.getStartDate(),job.getEndDate(),11);

        List<SubJob> subJobs = subJobManagement.makeSubJobs(dates,job);
        for(SubJob subJob : subJobs){
            subJob.setCommand(findCommand.findCommand(subJob,job));
        }
        job.setSubJobs(subJobs);



        Thread thread = new Thread(() -> {
                try {
                    for (SubJob subJob : job.getSubJobs()) {
                        if(runReady(60000)){
                        subJobManagement.run(subJob,job);
                    }
                    }
                } catch (InterruptedException | IOException e) {
                    e.printStackTrace();
                }
            });
            thread.start();


            return (String.valueOf(jobManager.gsonLogger(job)) + "\n Please Note the Job ID for checking status ");

    }
    public Job jobDetails(int jobuid) throws IOException {
        List<Job> jobs = jobManager.readGson();
        for(Job job : jobs){
            if(job.getJobuuid()==jobuid){
                System.out.println(job);
                return job;
            }
        }
        return null;
    }

    public String forceRun(int jobuid) throws IOException, ParseException {
        List<Job> jobs = jobManager.readGson();
        Job reRunJob = null;
        for(Job job : jobs) {
            if (job.getJobuuid() == jobuid) {
                reRunJob = job;
                jobs.remove(job);
                break;
            }
        }

                JobList jobList = new JobList();
                jobList.setJobList(jobs);
                try {
                    jobManager.writeGSON(jobList);
                } catch (IOException e) {
                    e.printStackTrace();
                }


        return startJob(reRunJob.getInstId(), reRunJob.getStartDate(), reRunJob.getEndDate(), reRunJob.getCollection());
    }

    public boolean runReady(long watchInterval){
        try {
            while(true){
                int running = jobManager.listJobs();
                if ( running <= 6 && running >= 0 ) {
                    return true;
                }
                Thread.sleep(watchInterval);
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }
}
