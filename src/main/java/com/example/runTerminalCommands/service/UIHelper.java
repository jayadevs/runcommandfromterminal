package com.example.runTerminalCommands.service;

import com.example.runTerminalCommands.model.Job;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.*;

@Service
public class UIHelper {
    @Autowired
    SplitJobService splitJobService;
    public Map getAllInst() {
        JSONParser jsonParser = new JSONParser();
        try (FileReader reader = new FileReader("ReconConfig.json")) {
            Object obj = jsonParser.parse(reader);
            JSONObject jsonObject = (JSONObject) obj;
            Set<String> keys = jsonObject.keySet();
            Set<String> collections = null;
            Map<String,List<String>> maps = new HashMap<>();
            for (String key : keys) {
                if (maps.get(key.substring(0, key.indexOf("_"))) == null) {
                    List<String> collexn = new ArrayList<>();
                    collexn.add(key.substring(key.indexOf("_")+1, key.length()));
                    maps.put(key.substring(0, key.indexOf("_")), collexn);
                } else {
                    List<String> collexn = maps.get(key.substring(0, key.indexOf("_")));
                    collexn.add(key.substring(key.indexOf("_")+1, key.length()));
                    maps.replace(key.substring(0, key.indexOf("_")), collexn);
                }
            }
            return maps;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
    public Map getAllInstId() {
        JSONParser jsonParser = new JSONParser();
        try (FileReader reader = new FileReader("DataDictionaryConfig.json")) {
            Object obj = jsonParser.parse(reader);
            JSONObject jsonObject = (JSONObject) obj;
            JSONObject anotherObject = (JSONObject) jsonObject.get("config");
            Set<String> keys = anotherObject.keySet();
            Set<String> collections = null;
            Map<String,List<String>> maps = new HashMap<>();
            for (String key : keys) {
                if (maps.get(key.substring(0, key.indexOf("_"))) == null) {
                    List<String> collexn = new ArrayList<>();
                    collexn.add(key.substring(key.indexOf("_")+1, key.length()));
                    maps.put(key.substring(0, key.indexOf("_")), collexn);
                } else {
                    List<String> collexn = maps.get(key.substring(0, key.indexOf("_")));
                    collexn.add(key.substring(key.indexOf("_")+1, key.length()));
                    maps.replace(key.substring(0, key.indexOf("_")), collexn);
                }
            }
            return maps;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    public List getCollections(String institution) {
        JSONParser jsonParser = new JSONParser();
        try (FileReader reader = new FileReader("ReconConfig.json")) {
            Object obj = jsonParser.parse(reader);
            JSONObject jsonObject = (JSONObject) obj;
            Set<String> keys = jsonObject.keySet();
            Set<String> collections = null;
            for (String key : keys) {
                if (key.substring(0, key.indexOf("_")).equals(institution)) {
                    collections.add(key.substring(key.indexOf("_"), key.length()));

                }
                return Arrays.asList(collections.toArray());

            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
    public List<String> inputParse(Job job){
        List<String> returnable= new ArrayList<>();
        returnable.add(job.getInstId());
        returnable.add(splitJobService.anotherParseDate(job.getStartDate()));
        returnable.add(splitJobService.anotherParseDate(job.getEndDate()));
//        int collexn= Integer.parseInt(job.getCollection());
//        List collexnx = (List) getAllInst().get(job.getInstId());
//        returnable.add((String) collexnx.get(collexn));
        String collection = job.getCollection();
        returnable.add(collection);
        return returnable;
    }
}
