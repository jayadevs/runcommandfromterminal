package com.example.runTerminalCommands.service;

import com.example.runTerminalCommands.model.DataDictionarySyncRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org  .springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.Duration;
import java.util.Date;

@Service
public class DataDictionaryService {
    @Autowired
    RestTemplate restTemplate;
    public String syncDataDictionary(DataDictionarySyncRequest dataDictionarySyncRequest) throws JsonProcessingException, UnirestException, FileNotFoundException {
        ObjectMapper mapper = new ObjectMapper();
        System.out.println(dataDictionarySyncRequest);
        Unirest.setTimeouts(0, 0);
        HttpResponse<String> response = Unirest.post(getUrl(dataDictionarySyncRequest))
                .header("Content-Type", "application/json")
                .body(mapper.writeValueAsString(dataDictionarySyncRequest))
                .asString();
        if(response.getStatus()!=200){
            return "Sync unsuccessful";
        }
        return response.getBody();
    }

    public String getUrl(DataDictionarySyncRequest dataDictionarySyncRequest) throws FileNotFoundException {
        JSONParser jsonParser = new JSONParser();
        FileReader reader = new FileReader("DataDictionaryConfig.json");
        try {
            JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
            JSONObject configObject = (JSONObject) jsonObject.get("config");
            JSONObject urlObject = (JSONObject) jsonObject.get("urls");
            JSONObject keyObject = (JSONObject) configObject.get(dataDictionarySyncRequest.getInstitutionId()+"_"+dataDictionarySyncRequest.getCollectionName());
            String urlKey = (String) keyObject.get("url");
            String url = (String) urlObject.get(urlKey);
            return url;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean checkValid(DataDictionarySyncRequest dataDictionarySyncRequest){
        if(dataDictionarySyncRequest.getInstitutionId()==null || dataDictionarySyncRequest.getCollectionName()==null || dataDictionarySyncRequest.getStartDate()==null || dataDictionarySyncRequest.getEndDate()==null){
            return false;
        }
        if(dataDictionarySyncRequest.getStartDate().after(dataDictionarySyncRequest.getEndDate())){
            return false;
        }
        return true;
    }

}
