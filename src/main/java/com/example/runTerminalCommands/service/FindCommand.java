package com.example.runTerminalCommands.service;

import com.example.runTerminalCommands.model.Job;
import com.example.runTerminalCommands.model.SubJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FindCommand {
    @Autowired
    ConfigFile configFile;
    public String findCommand(SubJob subJob,Job job){
        String instId= job.getInstId();
        String script="";
        String template = configFile.getCommand(job,instId+"_"+job.getCollection());
        String command = template.replace("StartDate", subJob.getStartDate());
        String finalCommand = command.replace("EndDate", subJob.getEndDate());
        script = finalCommand.replace("logfile", subJob.getLogFile());
        return script;
    }
}
