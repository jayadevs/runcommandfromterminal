package com.example.runTerminalCommands.service;

import com.example.runTerminalCommands.model.Job;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

@Service
public class ConfigFile {

    public String getCommand(Job job,String key){
        return readPropFile(key);
    }
    public String  readPropFile(String key){
        JSONParser jsonParser = new JSONParser();
        try(FileReader reader = new FileReader("ReconConfig.json")){
            Object obj = jsonParser.parse(reader);
            JSONObject jsonObject = (JSONObject) obj;
            String command = (String) jsonObject.get(key);
            return command;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
    public String getLogPath(Job job){
        JSONParser jsonParser = new JSONParser();
        try(FileReader reader = new FileReader("ReconLogPath.json")){
            Object obj = jsonParser.parse(reader);
            JSONObject jsonObject = (JSONObject) obj;
            String command = (String) jsonObject.get(job.getInstId());
            return command;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
