package com.example.runTerminalCommands.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class JobList {
    public JobList() {
        this.jobList = Collections.synchronizedList(new ArrayList<>());
    }

    private List<Job> jobList;

    public List<Job> getJobList() {
        return jobList;
    }

    public void setJobList(List<Job> jobList) {
        this.jobList = jobList;
    }
    public void add(Job job){
        this.jobList.add(job);
    }
    public void remove(Job job){
        this.jobList.remove(job);
    }
}
