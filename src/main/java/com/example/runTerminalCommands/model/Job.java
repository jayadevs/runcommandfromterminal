package com.example.runTerminalCommands.model;

import org.springframework.boot.autoconfigure.domain.EntityScan;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class Job implements Serializable {
    private int jobuuid;

    private String instId;
    private String startDate;
    private String endDate;
    private List<SubJob> subJobs;
    private String collection;
    private float status;

    public long getTimeOfSubmission() {
        return timeOfSubmission;
    }

    private long timeOfSubmission;

    @Override
    public String toString() {
        return "Job{" +
                "jobuuid=" + jobuuid +
                ", instId='" + instId + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", subJobs=" + subJobs +
                ", collection='" + collection + '\'' +
                ", timeOfSubmission=" + timeOfSubmission +
                '}';
    }




    public List<SubJob> getSubJobs() {
        return subJobs;
    }

    public void setSubJobs(List<SubJob> subJobs) {
        this.subJobs = subJobs;
    }

    public String getInstId() {
        return instId;
    }

    public void setInstId(String instId) {
        this.instId = instId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Job(String instId, String startDate, String endDate, String collection) {
        this.instId = instId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.collection = collection;
        this.jobuuid =this.hashCode();
        this.timeOfSubmission = System.currentTimeMillis();
    }

    public Job() {
    }

    public String getCollection() {
        return collection;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Job job = (Job) o;
        return jobuuid ==(job.jobuuid) && instId.equals(job.instId ) &&  startDate.equals(job.startDate) && endDate.equals(job.endDate) && collection.equals(job.collection);
    }

    @Override
    public int hashCode() {
        return Objects.hash(instId, startDate, endDate, collection);
    }

    public int getJobuuid() {
        return jobuuid;
    }

    public void setJobuuid(int jobuuid) {
        this.jobuuid = jobuuid;
    }


}
