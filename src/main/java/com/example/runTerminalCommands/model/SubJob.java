package com.example.runTerminalCommands.model;

public class SubJob {
    private String startDate;
    private String endDate;
    private String command;
    private String logFile;
    private final int jobuid;
    private String applicationId;

    @Override
    public String toString() {
        return "SubJob{" +
                "startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", command='" + command + '\'' +
                ", logFile='" + logFile + '\'' +
                ", applicationId='" + applicationId + '\'' +
                '}';
    }

    public int getJobuid() {
        return jobuid;
    }

    public SubJob(String startDate, String endDate, int jobuid) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.jobuid = jobuid;
        this.logFile= "recon"+this.jobuid+this.startDate+this.endDate+".log";
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getLogFile() {
        return logFile;
    }

    public void setLogFile() {
        this.logFile = "recon"+this.jobuid+this.startDate+this.endDate+".log";
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String  applicationId) {
        this.applicationId = applicationId;
    }
}
