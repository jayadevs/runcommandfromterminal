package com.example.runTerminalCommands.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SplitJobServiceTest {
    @Autowired
    SplitJobService splitJobService;
    @Test
    void findDaysBetween() {
        SplitJobService splitJobService = new SplitJobService();
        int days = splitJobService.findDaysBetween("01012020","02012021");
        assertEquals(days,2);
    }
    @Test
    void parseDate(){
        String date = "01052020";
        SplitJobService splitJobService = new SplitJobService();
        String parsedDate = splitJobService.parseDate(date);
        assertEquals(parsedDate,"2020-05-01");
    }
    @Test
    void anotherParseDate(){
        String date = "2020-05-01";
        SplitJobService splitJobService = new SplitJobService();
        String parsedDate = splitJobService.anotherParseDate(date);
        assertEquals(parsedDate,"01052020");
    }

    @Test
    void getDateAsString(){
        LocalDate date = LocalDate.parse("2020-05-01");
        SplitJobService splitJobService = new SplitJobService();
        String stringDate = splitJobService.getDateAsString(date);
        assertEquals(stringDate,"01052020");
    }



//    @Test
//    void dateIterator() {
//        SplitJobService splitJobService = new SplitJobService();
//        List<String> dates = splitJobService.dateIterator("01012020","10012020",3);
//        assertEquals(dates.size(),8);
//    }


}