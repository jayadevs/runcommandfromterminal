package com.example.runTerminalCommands.service;

import com.example.runTerminalCommands.model.Job;
import com.example.runTerminalCommands.model.JobList;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class JobManagerTest {
    @Autowired
    JobManager jobManager;
    @Test
    void makeJob() {
        JobManager jobManager = new JobManager();
        Job testjJob = jobManager.makeJob("HDFC OD", "01012020","06012020","goNoGoCustomerApplication");
        assertEquals(testjJob.getInstId(),"HDFC OD");
        assertEquals(testjJob.getStartDate(),"01012020");
        assertEquals(testjJob.getEndDate(),"06012020");
        assertEquals(testjJob.getCollection(),"goNoGoCustomerApplication");
    }

}