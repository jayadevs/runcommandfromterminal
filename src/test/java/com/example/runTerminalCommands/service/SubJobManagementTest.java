package com.example.runTerminalCommands.service;

import com.example.runTerminalCommands.model.Job;
import com.example.runTerminalCommands.model.SubJob;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SubJobManagementTest {

    @Autowired
    private SubJobManagement subJobManagement;

   @Before
   public void setup(){
       MockitoAnnotations.initMocks(this);
       SubJobManagement subJobManagement = new SubJobManagement();
   }



    @Test
    void makeSubJobs() {
        List<String> dates = new ArrayList<>();
        dates.add("01012020");
        dates.add("03012020");
        dates.add("04012020");
        dates.add("06012020");
        assertNotNull(dates);
        Job jobMock = new Job("HDFC OD","01012020","06012020","goNoGOCustomerApplication");
        List<SubJob> subJobs = new ArrayList<>();
        SubJobManagement subJobManagement = new SubJobManagement();
        subJobs=subJobManagement.makeSubJobs(dates,jobMock);
        assertNotNull(subJobs);
        assertEquals(subJobs.size(),2);
    }
}